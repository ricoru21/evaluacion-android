package com.buddyoruna.android.redline;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

public class HomeActivity extends AppCompatActivity {

    private AppCompatButton btn_empezar;
    private EditText edit_name,edit_ciclo;
    private WandoSession session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        edit_name = (EditText) findViewById(R.id.edit_name);
        edit_ciclo =(EditText) findViewById(R.id.edit_ciclo);

        btn_empezar = (AppCompatButton) findViewById(R.id.btn_empezar);
        btn_empezar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.values.nombre_participante=edit_name.getText().toString();
                session.values.ciclo_participante=edit_ciclo.getText().toString();
                session.updateSession();
                Intent intent = new Intent(HomeActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        init();
    }

    private void init(){
        session = WandoSession.getSession(getApplicationContext());
        session.values.numero_aciertos = 0;
        session.values.puntaje_actual = 0;
        session.values.nombre_participante="";
        session.updateSession();
    }

}
