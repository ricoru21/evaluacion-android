package com.buddyoruna.android.redline;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

/**
 * Created by Ricoru on 3/10/16.
 */

public class WandoSession {

    final static String TAG = WandoSession.class.toString();
    public static final String PREFERENCES_NAME = "com.buddyoruna.android.cuestionario";
    private static String KEY = "wandosession.cuestionario";

    private Context context;
    public SessionData values;

    private WandoSession(Context context){
        this.context = context;
    }

    private static WandoSession instance;
    public static WandoSession getSession(Context context){
        if (instance != null)
            return instance;

        instance = new WandoSession(context);
        instance.loadSession();
        return instance;
    }

    private void loadSession(){
        String jsonString = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE).getString(KEY, "");
        if ("".equals(jsonString)) {
            values = new SessionData();
        }else{
            values = (SessionData) new Gson().fromJson(jsonString, SessionData.class);
        }
    }

    public void updateSession(){
        try{
            String jsonString =  new Gson().toJson(values);
            SharedPreferences loSharedPreferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor loEditor = loSharedPreferences.edit();
            loEditor.putString(KEY, jsonString);
            loEditor.commit();
        }catch(Exception e){
            Log.d(TAG, "Error al actualizar los datos de la sesión");
        }
    }

    public void clearSession(){
    }

}
