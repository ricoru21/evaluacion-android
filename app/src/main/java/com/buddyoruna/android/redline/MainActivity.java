package com.buddyoruna.android.redline;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;

public class MainActivity extends AppCompatActivity implements PreguntasFragment.PreguntasFragmentListener{

    private PreguntasFragment preguntasFragment;
    private int pregunta_actual=0;

    private WandoSession session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState==null){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            preguntasFragment
                    = PreguntasFragment.newInstance(pregunta_actual);
            ft.add(R.id.frag_content_pregunta, preguntasFragment).commit();
        }

        init();
    }

    private void init(){
        session = WandoSession.getSession(getApplicationContext());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public void onContinuar(int puntaje, int numero_acierto) {
        session.values.numero_aciertos = session.values.numero_aciertos + numero_acierto;
        session.values.puntaje_actual = session.values.puntaje_actual+puntaje;
        session.updateSession();
        pregunta_actual++;
        if(pregunta_actual>=25){
            Intent intent = new Intent(MainActivity.this, ResultadoActivity.class);
            startActivity(intent);
            finish();
            //Finalizo la encuesta y lelvar a una nueva actividad
        }else {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            preguntasFragment
                    = PreguntasFragment.newInstance(pregunta_actual);
            ft.replace(R.id.frag_content_pregunta, preguntasFragment).commit();
        }

    }

}
