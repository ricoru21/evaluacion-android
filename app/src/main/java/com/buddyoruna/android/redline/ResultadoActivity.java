package com.buddyoruna.android.redline;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

public class ResultadoActivity extends AppCompatActivity {

    private WandoSession session;
    private Typeface fonttype;

    private TextView txt_nro_aciertos,txt_nro_fallas,txt_titulo_resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);
        txt_titulo_resultado = (TextView) findViewById(R.id.txt_titulo_resultado);
        txt_nro_aciertos = (TextView) findViewById(R.id.txt_nro_aciertos);
        txt_nro_fallas = (TextView) findViewById(R.id.txt_nro_fallas);
        init();
    }

    private void init(){
        session = WandoSession.getSession(getApplicationContext());
        fonttype = Typeface.createFromAsset(getApplication().getAssets(), "fonts/digital_7.ttf");
        txt_nro_aciertos.setTypeface(fonttype);
        txt_nro_fallas.setTypeface(fonttype);

        try {
            int numero_aciertos = session.values.numero_aciertos;
            txt_nro_aciertos.setText(String.valueOf(numero_aciertos));
            txt_nro_fallas.setText(String.valueOf(25-numero_aciertos));
            txt_titulo_resultado.setText("SCORE - " + session.values.nombre_participante);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.resultado_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_finish:
                Intent intent = new Intent(ResultadoActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
