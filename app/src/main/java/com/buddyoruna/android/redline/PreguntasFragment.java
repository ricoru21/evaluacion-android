package com.buddyoruna.android.redline;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Ricoru on 13/10/16.
 */

public class PreguntasFragment extends Fragment {

    private static String ARGUMENT_DATA_NRO_PREGUNTA="data_nro_pregunta";

    private TextView txt_titulo_pregunta;
    private AppCompatButton btn_opcion_1,btn_opcion_2,btn_opcion_3,btn_opcion_4;

    private int nro_pregunta;
    private int repsuesta_seleccionada=-1;
    private boolean flag_select=false;

    public static PreguntasFragment newInstance(int nro_pregunta) {
        PreguntasFragment fragment = new PreguntasFragment();
        Bundle args = new Bundle();
        args.putInt(ARGUMENT_DATA_NRO_PREGUNTA,nro_pregunta);
        fragment.setArguments(args);
        return fragment;
    }

    public PreguntasFragment(){
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_preguntas, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        txt_titulo_pregunta = (TextView) getActivity().findViewById(R.id.txt_titulo_pregunta);
        btn_opcion_1 = (AppCompatButton) getActivity().findViewById(R.id.btn_opcion_1);
        btn_opcion_2 = (AppCompatButton) getActivity().findViewById(R.id.btn_opcion_2);
        btn_opcion_3 = (AppCompatButton) getActivity().findViewById(R.id.btn_opcion_3);
        btn_opcion_4 = (AppCompatButton) getActivity().findViewById(R.id.btn_opcion_4);

        btn_opcion_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!flag_select) {
                    repsuesta_seleccionada = 1;
                    flag_select = true;
                    btn_opcion_1.setBackgroundDrawable(getResources().getDrawable(R.color.colorAmber_500));
                    btn_opcion_1.getBackground().setColorFilter(getResources().getColor(R.color.colorAmber_500), PorterDuff.Mode.MULTIPLY);
                    btn_opcion_1.setTextColor(getResources().getColor(R.color.colorWhite));
                }else{
                    Toast.makeText(getActivity(),"No se pueden seleccionar mas de 1 Respuesta",Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn_opcion_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!flag_select) {
                    repsuesta_seleccionada = 2;
                    flag_select = true;
                    btn_opcion_2.setBackgroundDrawable(getResources().getDrawable(R.color.colorAmber_500));
                    btn_opcion_2.getBackground().setColorFilter(getResources().getColor(R.color.colorAmber_500), PorterDuff.Mode.MULTIPLY);
                    btn_opcion_2.setTextColor(getResources().getColor(R.color.colorWhite));
                }else{
                    Toast.makeText(getActivity(),"No se pueden seleccionar mas de 1 Respuesta",Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn_opcion_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!flag_select) {
                    repsuesta_seleccionada = 3;
                    flag_select = true;
                    btn_opcion_3.setBackgroundDrawable(getResources().getDrawable(R.color.colorAmber_500));
                    btn_opcion_3.getBackground().setColorFilter(getResources().getColor(R.color.colorAmber_500), PorterDuff.Mode.MULTIPLY);
                    btn_opcion_3.setTextColor(getResources().getColor(R.color.colorWhite));
                }else{
                    Toast.makeText(getActivity(),"No se pueden seleccionar mas de 1 Respuesta",Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn_opcion_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!flag_select) {
                    repsuesta_seleccionada = 4;
                    flag_select = true;
                    btn_opcion_4.setBackgroundDrawable(getResources().getDrawable(R.color.colorAmber_500));
                    btn_opcion_4.getBackground().setColorFilter(getResources().getColor(R.color.colorAmber_500), PorterDuff.Mode.MULTIPLY);
                    btn_opcion_4.setTextColor(getResources().getColor(R.color.colorWhite));
                }else{
                    Toast.makeText(getActivity(),"No se pueden seleccionar mas de 1 Respuesta",Toast.LENGTH_SHORT).show();
                }
            }
        });

        if(savedInstanceState==null){
            init();
        }

    }

    private void init(){

        if(getArguments()!=null){
            nro_pregunta = getArguments().getInt(ARGUMENT_DATA_NRO_PREGUNTA);
            txt_titulo_pregunta.setText(getResources().getStringArray(R.array.preguntas)[nro_pregunta]);
            Log.i("INFO","nro_pregunta: "+ nro_pregunta);
            switch (nro_pregunta){
                case 0:
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_1)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_1)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_1)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_1)[3]);
                    break;
                case 1:
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_2)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_2)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_2)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_2)[3]);
                    break;
                case 2:
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_3)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_3)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_3)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_3)[3]);
                    break;
                case 3:
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_4)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_4)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_4)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_4)[3]);
                    break;
                case 4:
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_5)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_5)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_5)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_5)[3]);
                    break;
                case 5:
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_6)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_6)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_6)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_6)[3]);
                    break;
                case 6:
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_7)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_7)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_7)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_7)[3]);
                    break;
                case 7:
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_8)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_8)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_8)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_8)[3]);
                    break;
                case 8:
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_9)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_9)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_9)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_9)[3]);
                    break;
                case 9:
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_10)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_10)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_10)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_10)[3]);
                    break;
                case 10:
                    Log.i("INFO","ingreso a aqui recurso 11");
                    Log.i("INFO","ingreso :: "+ getResources().getStringArray(R.array.pregunta_11)[0]);
                    Log.i("INFO","ingreso :: "+ getResources().getStringArray(R.array.pregunta_11)[3]);
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_11)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_11)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_11)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_11)[3]);
                    break;
                case 11:
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_12)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_12)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_12)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_12)[3]);
                    break;
                case 12:
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_13)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_13)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_13)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_13)[3]);
                    break;
                case 13:
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_14)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_14)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_14)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_14)[3]);
                    break;
                case 14:
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_15)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_15)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_15)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_15)[3]);
                    break;
                case 15:
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_16)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_16)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_16)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_16)[3]);
                    break;
                case 16:
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_17)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_17)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_17)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_17)[3]);
                    break;
                case 17:
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_18)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_18)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_18)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_18)[3]);
                    break;
                case 18:
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_19)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_19)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_19)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_19)[3]);
                    break;
                case 19:
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_20)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_20)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_20)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_20)[3]);
                    break;
                case 20:
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_21)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_21)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_21)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_21)[3]);
                    break;
                case 21:
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_22)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_22)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_22)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_22)[3]);
                    break;
                case 22:
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_23)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_23)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_23)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_23)[3]);
                    break;
                case 23:
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_24)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_24)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_24)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_24)[3]);
                    break;
                case 24:
                    btn_opcion_1.setText(getResources().getStringArray(R.array.pregunta_25)[0]);
                    btn_opcion_2.setText(getResources().getStringArray(R.array.pregunta_25)[1]);
                    btn_opcion_3.setText(getResources().getStringArray(R.array.pregunta_25)[2]);
                    btn_opcion_4.setText(getResources().getStringArray(R.array.pregunta_25)[3]);
                    break;

            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //finish();
                return true;
            case R.id.action_next:
                //continuar
                continuar();
                Log.i("INFO","continuar ");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void continuar(){
        if(!flag_select){
             Toast.makeText(getActivity(),"Tiene que seleccionar una respuesta!!",Toast.LENGTH_SHORT).show();
        }else{
            if(repsuesta_seleccionada == getResources().getIntArray(R.array.repsueta_preguntas)[nro_pregunta]){
                mListener.onContinuar(4,1);
            }else {
                mListener.onContinuar(0,0);
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (PreguntasFragmentListener) activity;
        } catch (ClassCastException e) {
            e.printStackTrace();
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    private PreguntasFragmentListener mListener;
    public interface PreguntasFragmentListener {
        public void onContinuar(int puntaje, int numero_acierto);
    }

}
